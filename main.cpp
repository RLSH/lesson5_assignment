#include <iostream>
#include <stdio.h>
#include <Windows.h>
#include <string>
#include <algorithm>
#include <iterator>
#include "Helper.h"

#define TRUE	      1
#define FALSE	      0
#define PATH_MAX      2048

#define FAIL_OF_PWD   0

#define FAIL_TO_EXE   31

HANDLE hEvent;

typedef int(__stdcall *f_TheAnswerToLife)();

std::vector<std::string> whatCommad(LPCSTR command);
std::vector<std::string> get_all_files_names_within_folder(std::string folder);

int check(LPCSTR input, std::string funciton, int exceptedSize);
int checkIfExe(LPCSTR input);

int pwdFunc(char buffer[]);
int cdFunc(LPCSTR fakeStr);
int createFunc(LPCSTR fakeStr);
int lsFunc(char buffer[]);
int secretFunc(char buffer[]);
int exeFunc(LPCSTR fakeStr);


int main()
{
	std::string str = "";
	std::string output = "";
	char buffer[MAX_PATH];
	int i = 0;
	int ans = 0;

	LPCSTR fakeStr;
	DWORD pathSize = PATH_MAX;
	
	//run for ever
	while (TRUE)
	{
		std::cout << ">> ";
		//get input of the user
		std::getline(std::cin, str);
		fakeStr = (LPCSTR)str.c_str();

		//for every option, exist, check if it exist
		//in every function there is return value, if 0, all fine, if 1, error
		if (check(fakeStr, "pwd", 1))
		{
			ans = pwdFunc(buffer);
		}
		else if (check(fakeStr, "cd", 1))
		{
			ans = cdFunc(fakeStr);
		}
		else if (check(fakeStr, "create", 1))
		{
			ans = createFunc(fakeStr);
		}
		else if (check(fakeStr, "ls", 1))
		{
			ans = lsFunc(buffer);
		}
		else if (check(fakeStr, "secret", 1))
		{
			ans = secretFunc(buffer);
		}
		else if (checkIfExe(fakeStr))
		{
			ans = exeFunc(fakeStr);
		}

	}

	system("PAUSE");
	return 0;
}




//this function discover what is the command that the user entered by helper functions
std::vector<std::string> whatCommad(LPCSTR command)
{
	std::string str = std::string(command);
	Helper::trim(str);

	//return the vector of all the command
	return Helper::get_words(str);
}

/*
this function discover all the names of the files in a foldar
input:  string - foldar: the foldar
output: the vector of all the names
*/
std::vector<std::string> get_all_files_names_within_folder(std::string folder)
{
	std::vector<std::string> names;
	std::string search_path = folder + "/*.*";
	WIN32_FIND_DATA fd;
	HANDLE hFind = ::FindFirstFile(search_path.c_str(), &fd);

	//if the handle is working
	if (hFind != INVALID_HANDLE_VALUE) {
		do {
			// read all (real) files in current folder
			// , delete '!' read other 2 default folder . and ..
			if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
				names.push_back(fd.cFileName);
			}
		} while (::FindNextFile(hFind, &fd));
		::FindClose(hFind);
	}
	//if the handle is not working show the problam
	else
	{
		std::cout << "Failed to files in directory" << std::endl;
		std::cout << "error: " + GetLastError() << std::endl;

		system("PAUSE");
	}

	return names;
}

/*
this function check for each command, if is this the command
input:  LPCSTR - input: the command
		string - function: check if this is the command
		int - exceptedSize: the size the command should be
*/
int check(LPCSTR input, std::string function, int exceptedSize)
{
	//get the command
	std::vector<std::string> command = whatCommad(input);

	//check the size and if the first word is the command
	if (command.size() >= exceptedSize && command[0] == function)
	{
		//if correct return true
		return TRUE;
	}

	return FALSE;
}


/*
special check to the exe because it have special ending
input:  LPCSTR - input: the command
output: correct ot not
*/
int checkIfExe(LPCSTR input)
{
	std::vector<std::string> command = whatCommad(input);

	if (command.size() == 1 && command[0].length() > 4)
	{
		std::string str = command[0];
		int len = str.length();

		if (str[len - 1] == 'e' && str[len - 2] == 'x' && str[len - 3] == 'e' && str[len - 4] == '.')
		{
			return TRUE;
		}
	}

	return FALSE;
}


/*
this funciton print the path of the directory that the shell open in
input:  char* - buffer: the buffer
output: return 1 for fail and 0 for success
*/
int pwdFunc(char buffer[])
{
	//try to get the directory
	GetCurrentDirectory(MAX_PATH, buffer);
	DWORD retVal = GetLastError();

	if (retVal = FAIL_OF_PWD)
	{
		//if failed print message
		std::cout << "Failed to show path" << std::endl;
		std::cout << "error: " << GetLastError() << std::endl;

		system("PAUSE");

		//return 1
		return 1;
	}

	//discover the directory and print it
	std::string f = std::string(buffer);
	f.substr(0, f.find_last_of("\\/"));
	std::cout << f << std::endl;

	//return all good
	return 0;
}


/*
this function do the cd function
input:  LPCSTR - fakeStr: the command
output: 1/0
*/
int cdFunc(LPCSTR fakeStr)
{
	//if failed print the message
	if (!SetCurrentDirectory((LPCSTR)whatCommad(fakeStr)[1].c_str()))
	{
		std::cout << "Failed to change dir" << std::endl;
		std::cout << "error: " << GetLastError() << std::endl;

		system("PAUSE");
		return 1;
	}

	//retur all good
	return 0;
}

/*
this is the create file function
input:  LPCSTR fakeStr: the command
output: 1/0
*/
int createFunc(LPCSTR fakeStr)
{
	HANDLE hfile = CreateFile((LPCSTR)whatCommad(fakeStr)[1].c_str(), GENERIC_READ, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);

	//if this file, exist, delete it and create again
	if (hfile == INVALID_HANDLE_VALUE)
	{
		DeleteFile((LPCSTR)whatCommad(fakeStr)[1].c_str());
		HANDLE hfile = CreateFile((LPCSTR)whatCommad(fakeStr)[1].c_str(), GENERIC_READ, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
	}

	//close the handle
	CloseHandle(hfile);

	//return all good
	return 0;
}


/*
this is the ls function
input:  char* buffer: the buffer
output: 1/0
*/
int lsFunc(char buffer[])
{
	int i = 0;
	GetCurrentDirectory(MAX_PATH, buffer);

	DWORD retVal = GetLastError();

	//if failed, print the message
	if (retVal = FAIL_OF_PWD)
	{
		std::cout << "Failed to get path" << std::endl;
		std::cout << "error: " << GetLastError() << std::endl;

		system("PAUSE");
		return 1;
	}

	//get all the files names
	std::string f = std::string(buffer);
	f.substr(0, f.find_last_of("\\/"));
	std::vector<std::string> vec = get_all_files_names_within_folder(std::string(buffer));

	if (vec.size() == 0)
	{
		return 1;
	}

	//print all the files names
	for (i = 0; i < vec.size(); i++)
	{
		std::cout << vec[i] + "  ";
	}

	std::cout << std::endl;

	//all is good
	return 0;
}


/*
this is the secret function
input:  char* buffer: the buffer
output: 1/0
*/
int secretFunc(char buffer[])
{
	GetCurrentDirectory(MAX_PATH, buffer);

	DWORD retVal = GetLastError();

	//if failed  print the message
	if (retVal = FAIL_OF_PWD)
	{
		std::cout << "Failed to get path" << std::endl;
		std::cout << "error: " << GetLastError() << std::endl;

		system("PAUSE");
		return 1;
	}

	//open the dll file withe handles
	std::string f = std::string(buffer) + "\\Secret.dll";
	f.substr(0, f.find_last_of("\\/"));
	HINSTANCE hGetProcIDDLL = LoadLibrary((LPCSTR)f.c_str());

	//if cant open
	if (!hGetProcIDDLL) {
		std::cout << "could not load the dynamic library" << std::endl;
		std::cout << "error: " + GetLastError() << std::endl;
		system("PAUSE");

		return 1;
	}

	// resolve function address here
	f_TheAnswerToLife function = (f_TheAnswerToLife)GetProcAddress(hGetProcIDDLL, "TheAnswerToLifeTheUniverseAndEverything");
	if (!function) {
		std::cout << "could not locate the function" << std::endl;
		std::cout << "error: " + GetLastError() << std::endl;
		system("PAUSE");

		return 1;
	}

	//print the function result
	std::cout << "TheAnswerToLifeTheUniverseAndEverything() returned " << function() << std::endl;


	//all is good
	return 0;
}


/*
this is the function that run exe files
input:  LPCSTR fakeStr: the command
output: 1/0
*/
int exeFunc(LPCSTR fakeStr)
{
	//handle to the exe
	HANDLE retVal = (HANDLE)WinExec(fakeStr, 6);

	//if failed print message
	if ((UINT)retVal <= FAIL_TO_EXE)
	{
		std::cout << "Failed to create procces" << std::endl;
		std::cout << "error: " << GetLastError() << std::endl;

		system("PAUSE");
		return 1;
	}

	//wait for object
	WaitForSingleObject(retVal, INFINITE);

	return 0;
}

