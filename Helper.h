#ifndef _HELPER_H
#define _HELPER_H

#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <sstream>

// this is service class with static functions
class Helper
{
public:
	// Remove whitespace from the begining and the end of the string
	static void trim(std::string &str);

	// Get a vector of strings, and return a vector of words 
	// ("for example" vec[0]:"for" vec[1]:"example"
	static std::vector<std::string> get_words(std::string &str);

private:
	// Remove whitespace from the end of the string
	static void rtrim(std::string &str);

	// Remove whitespace from the beginning of the string
	static void ltrim(std::string &str);


};

#endif